ARG IMAGE

# php:7.2.21-apache for 7.2
# php:7.3.7-apache for 7.3

FROM $IMAGE

RUN apt-get update && \
	apt-get install -y \
		curl wget procps zip git g++ pkg-config \
		libssl-dev zlib1g-dev libicu-dev libpq-dev libpng-dev \
		libbz2-dev libenchant-dev libgmp-dev libmagickwand-dev \
		libldb-dev libldap2-dev libmemcached-dev libtidy-dev libxslt-dev libzip-dev

RUN docker-php-ext-install \
	bcmath \
	bz2 \
	calendar \
	dba \
	enchant \
	exif \
	gd \
	gettext \
	gmp \
	intl \
	ldap \
	mysqli \
	pcntl \
	pdo_mysql \
	pdo_pgsql \
	pgsql \
	shmop \
	soap \
	sockets \
	sysvshm \
	tidy \
	xmlrpc \
	xsl \
	opcache \
	zend_test \
	zip


RUN pecl install \
	grpc-1.19.0 \
	imagick-3.4.3 \
	memcached-3.1.3 \
	mongodb-1.5.3 \
	opencensus-0.2.2 \
	protobuf-3.7.1 \
	redis-4.3.0 \
	stackdriver_debugger-0.2.0

COPY ./apache /etc/apache2/sites-available/000-default.conf
	
RUN sed -i 's/80/${PORT}/g' /etc/apache2/ports.conf && \
	mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini" && \
	a2enmod rewrite headers

RUN useradd -u 1000 -g www-data -m -s /bin/bash developer

RUN chown www-data:www-data /srv

RUN echo >> $PHP_INI_DIR/php.ini

WORKDIR /srv
